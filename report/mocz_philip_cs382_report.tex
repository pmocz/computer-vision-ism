%\voffset-.4in
\documentclass[usenatbib,usegraphicx,usedcolumn]{mn2e}
\usepackage{times,amssymb,amsmath}
\usepackage{relsize}
\newcommand{\remark}[1]{{\textcolor {red}{#1}}}
\usepackage[backref,breaklinks,colorlinks,citecolor=blue]{hyperref}
\usepackage[usenames,dvipsnames]{color}
\bibliographystyle{mn2e}

\setlength{\paperheight}{11in}
\setlength{\topmargin}{-1.2cm}

\def\apj{ApJ}
\def\mnras{MNRAS}
\def\nat{Nat}
\def\physrevB{Phys. Rev. B}
\def\araa{ARA\&A}                % "Ann. Rev. Astron. Astrophys."
\def\aap{A\&A}                   % "Astron. Astrophys."
\def\aaps{A\&AS}                 % "Astron. Astrophys. Suppl. Ser."
\def\aj{AJ}                      % "Astron. J."
\def\apjs{ApJS}                  % "Astrophys. J. Suppl. Ser."
\def\pasp{PASP}                  % "Publ. Astron. Soc. Pac."
\def\apjl{ApJ}                   % letter at ApJ
\def\pasj{PASJ}
\def\na{NA}
\def\physrep{Physics Reports}


\title[Characterizing the ISM using computer vision]{Computer vision techniques for structure identification in observations of the diffuse interstellar medium}
\author[Mocz]{Philip Mocz$^{1}$\thanks{E-mail: pmocz@cfa.harvard.edu. Code available at: \url{https://bitbucket.org/pmocz/computer-vision-ism}}  \\
Harvard-Smithsonian Center for Astrophysics, 60 Garden Street, Cambridge, MA 02138, USA \\
}
%###########################################################################################################
\begin{document}

\date{December 9, 2014}

\pagerange{\pageref{firstpage}--\pageref{lastpage}} \pubyear{2014}

\maketitle


\label{firstpage}
%###########################################################################################################

\begin{abstract}
We employ 3 computer vision feature identification and segmentation strategies to study the maps of neutral hydrogen in our Milky Way Galaxy's interstellar medium (ISM) taken by the Galactic Arecibo L-band Feed Array HI (GALFA-HI) Survey. (1) We implement and use a Rolling  Hough Transform (RHT) to characterize the strength and extent of coherent linear structures in the image. These linear structures, called fibers, trace the ISM magnetic fields. (2) We implement and use a Wavelet-based Image Segmentation and Evaluation (WISE) method for multiscale decomposition, segmentation, and identification of structural patterns in the image plane. This technique detects diffuse blob-like overdense regions and voids in the ISM at various scales. (3) We use the available Berkeley Segmentation Resources (BSR) image segmentation and boundary detection algorithm, which combines multiscale local brightness, color, and texture cues into a globalization framework based on spectral clustering. The method identifies patches of shocked regions in the gas. We produce valuable data products characterizing the structure and topology of the ISM using these 3 techniques and study the correlations between the different ISM visual features. In particular, we study the strength of the linear features (proxy for the magnetic field; identified by RHT) at different scales (identified by WISE), and show they increase at smaller scale overdensities. We also study the strength versus the volume filling factor of the linear features in the different shocks regions (identified by BSR), and find regions of higher volume filling factor have higher strength linear features as well. These findings characterize the turbulent nature of the ISM. The information-rich data products we produce are useful for future research.
\end{abstract}

\begin{keywords}
segmentation -- edge detection
\end{keywords}


\section{Introduction}\label{sec:intro}

The interstellar medium (ISM) is a complex system of supersonic turbulent, diffuse gas. A tractable way to describe and characterize its structure is to do so statistically. For example, such turbulent systems have power-law velocity power spectra (indicative of Kolmogorov turbulence) and log-normal density probability distribution functions \citep{1981MNRAS.194..809L,2009ApJ...699.1092H}. But features in the ISM are more complicated than can be described by these simple statistics. Turbulent systems can exhibit hierarchical structures, fractal like regions, and intermittent overdensities. In this work we study these types of visually-identifiable structures and their statistics in the ISM through the use of modern computer vision techniques.

Observations of hydrogen emission in the ISM reveal several features, from linear structures to multi-scale blobs. A challenging aspect of identifying these objects is that the ISM is a diffuse, partially transparent (optically thin) gas. Many computer vision techniques are not designed to handle this regime (instead they may focus on opaque objects, with Lambertian-like BRDFs). However, we present 3 modern computer vision, adapted for this particular application, that perform well in this optically thin regime. 

We use 3 techniques to identify features in the ISM. (1) We implemented a rolling Hough transform (RHT) to characterize the extent and strength of linear structures which trace the magnetic fields in the ISM. This method was recently developed by \cite{Clark2014} for astrophysical data. (2) We implemented a wavelet-based image segmentation and evaluation (WISE) for structure decomposition at multiple scales for partially transparent gas, described in \cite{Mertens2014}.  The method identifies significant structural patterns (SSP) and creates a segmented wavelet decomposition (SWD). The algorithm reveals blob-like features at different length scales. (3) We used the state-of-the-art contour detection and image segmentation algorithm of \cite{Arbelaez2010} to segment the image into different regions of shocked gas. The algorithm combines multiscale local brightness, color, and texture cues into a globalization framework based on spectral clustering for image segmentation and boundary detection.

The algorithms are used to identify features and create valuable data produces for future research that encode rich information about the structure and topology of the ISM which are not readily observable in the raw images. In addition, we study the statistics and correlation of different features identified by the computer vision algorithms. In particular we present results for the strength of the coherent linear structures a a function of the scale of the over-density in which they are embedded and also as a function of the the volume-filling factor of shocked regions.

From the 3 computer vision techniques, only the RHT has been applied in previous literature to study features in the ISM. \cite{Clark2014} apply the RHT to detect the orientation of fibers in the ISM and compare them with polarization measurements of the magnetic field. Detection of filamentary structures has been preformed by \cite{2003ApJS..149..365J} using Gaussian filters and finite difference approximations to the Hessian, which is another approach to characterize these linear structures. Advanced computer-vision techniques for blob and shocked-region detection have not been applied to the ISM. Furthermore, correlations between computer-vision identified structures have not been studied thus far. In this respect the methodology and scientific goals of the present work are novel.

The data come from the Galactic Arecibo L-Band Feed Array HI (GALFA-HI) Survey. The GALFA-HI survey maps neutral hydrogen in and around our Milky Way Galaxy with the Arecibo 305 meter telescope \cite{Peek2011}. The data is freely available online \footnote{\url{https://purcell.ssl.berkeley.edu/}}.

In \S~\ref{sec:data} we describe the data set. We describe the 3 computer vision techniques in \S~\ref{sec:methods}. Our results are presented in \S~\ref{sec:results}: namely, we describe the linear, blob-like, and shocked regions identified. In \S~\ref{sec:analysis} we analyze the statistics and correlations between some of these features. Finally, in \S~\ref{sec:conclusion} we offer our concluding remarks.



\begin{figure}
\centering
\includegraphics[width=0.47\textwidth]{figures/data}
\caption{This color image from the GALFA-HI survey (the dataset we use) is constructed from 3 velocity channels: $[-1.1,1.1]~{\rm km}~{\rm s}^{-1}$ (green; channel A), $[1.1,2.2]~{\rm km}~{\rm s}^{-1}$ (red; channel B), and $[-2.2,-1.1]~{\rm km}~{\rm s}^{-1}$ (blue; channel C), where the $(-)$ sign gas is moving towards the observer. The image is $512^2$ pixels, where the pixels represent $1$ arcminute right ascension (RA) and declination (dec) angular coordinates in the sky. The image is centered on RA$=60$, dec$=+26.35$.}
\label{fig:data}
\end{figure}

\section{Data}\label{sec:data}

Radio interferometers obtain 3 dimensional astrophysical data; namely, they obtain a spectra at each location of the image plane which can be used to construct a position-position-velocity datacube. That is, at each location of the image, we not only know the intensity of a pixel, but we know the intensity of the gas as a function of the line-of-sight velocity of the gas. The dataset used for this project consists of 3 velocity channels from the GALFA-HI survey: $[-1.1,1.1]~{\rm km}~{\rm s}^{-1}$ (channel A; green), $[1.1,2.2]~{\rm km}~{\rm s}^{-1}$ (channel B; red), and $[-2.2,-1.1]~{\rm km}~{\rm s}^{-1}$ (channel C; blue), where the $(-)$ sign gas is moving towards the observer. The instrument has resolution $0.18~{\rm km}~{\rm s}^{-1}$, so we have averaged a number of velocity slices to create each channel A,B, and C. The image in each channel is $512\times 512$ pixels, where the pixels represent $1$ arcminute of right ascension (RA) and declination (dec) angular coordinates in the sky. The image is centered on RA$=60$, dec$=+26.35$. This is just a small fraction of the GALFA-HI survey, and the analysis can be extended to the whole data set. Figure~\ref{fig:data} visualizes the input data into our computer vision analysis pipeline.


\begin{figure*}
\centering
\includegraphics[width=0.94\textwidth]{figures/RHTmethod}
\caption{A summary of the RHT algorithm from \protect\cite{Clark2014}. First, an unsharp mask of the image is created. It is then subtracted from the original image to  reveal linear features. The features are transformed into a bitwise map using a threshold value. The RHT is performed at each pixel of the image: the strength of having a line through the pixel with angle $\theta$ is calculated. Only strengths above some threshold value are kept.}
\label{fig:rhtmethod}
\end{figure*}

\begin{figure*}
\centering
\includegraphics[width=0.47\textwidth]{figures/wavelet_coeffs} 
\includegraphics[width=0.47\textwidth]{figures/wavelet_functions}
\caption{Wavelet coefficient representation of an image. The left panel shows the wavelet coefficients organized in Mallet's ordering. The bottom right quadrant, top right quadrant, and bottom left quadrant represent the coefficient for the diagonal, vertical, and horizontal wavelet basis functions respectively at the smallest spatial scale ($J_{\rm max}$). The coefficients for smaller larger spatial scales (smaller $j$ levels) are stored recursively in the upper left quadrant. The right panel illustrates the three types of biorthogonal basis functions used (diagonal, vertical, horizontal) for the wavelet transform.}
\label{fig:wc}
\end{figure*}


\section{Methods}\label{sec:methods}

All the {\sc Matlab} code for the methods employed, described in this section, are made publicly available online\footnote{\url{https://bitbucket.org/pmocz/computer-vision-ism}}. The repository also includes data files and processed results, for reproducibility.

\subsection{Rolling Hough Transform}\label{sec:RHT}

We describe here the RHT method used to identify linear features in the data. This method is a modification of the original Hough Transform \citep{Hough1962}. The method is designed to be sensitive to linear structure irrespective of the overall brightness of the region, making it ideal for study of the diffuse ISM \citep{Clark2014}. The first step is to unsharp mask the image. The image is convolved with a two-dimensional top-hat smoothing kernel of radius $R_K$. We choose $R_K=4$. The smoothed image is then subtracted from the original. Thus large-scale structures are suppressed in the process. The subtracted image is then converted to a bitwise map (positive values become $1$ and negative values become $0$).

The RHT calculates the strength that there is a line that crosses each pixel $(x,y)$ with angle $\theta$. This is accomplished by calculating the fraction of the pixels in a neighbourhood of radius $R_W$  around pixel $(x,y)$ that agree with the mask on a line through $(x,y)$ with angle $\theta$ inside the neighborhood region. We choose $R_W=32$. Only lines with that satisfy a minimum threshold agreement $Z$  ($Z\in[0,1]$) with the mask are considered significant. We choose $Z=0.6$.  We use the canonical binning of the number of $\theta$ bins:
\begin{equation}
N_\theta = \lceil\pi\frac{\sqrt{2}}{2}(D_W-1)\rceil
\end{equation}
Thus, the RHT transformation maps a 2D image of size $N_x\times N_y$ to a 3D space $R(\theta,x,y)$ of size $N_\theta\times N_x\times N_y$. The RHT output $R(\theta,x,y)$ may be visualized in 2D by calculating the back projection:
\begin{equation}
R(x,y) = \int R(\theta,x,y)\,d\theta
\end{equation}
The back projection is a measure of the strength of a linear feature at a given pixel. Alternatively, one may extract directionality of the linear feature by calculating the most likely value of $\theta$ at a given pixel.

The steps of the method are illustrated and summarized in Figure~\ref{fig:rhtmethod}.


\subsection{Wavelet-based Image Segmentation and Evaluation}\label{sec:WISE}

The second method we implement and employ is the WISE algorithm for identification of blob-like diffuse features at different spatial scales, as well as feature points. A version of the algorithm is presented in \cite{Mertens2014}. Our approach here is slightly modified for our application.

First, a wavelet transform is performed on the image. We use biorthogonal basis functions (diagonal, vertical, and horizontal), illustrated in Figure~\ref{fig:wc}. The wavelet transform operates as follows. The original image is of size $N\times N$. First, $3\times(N/2\times N/2)$ coefficients are obtained for the diagonal, vertical, and horizontal basis functions at scale $j=J_{\rm max}$, and one is left with a residual image of size $(N/2\times N/2)$. The operation is recursively applied to the residual image to obtain coefficients at larger spatial scales (smaller $j$), until a target level for $j$ (we use $j=3$) is reached. Each level $j$ corresponds to a spatial scale of $2^{-j}$.

Multiscale decomposition can be achieved using the wavelet coefficients, in a way that is very robust to the characteristic structural patterns of the image, and largely insensitive to noise \citep{Mertens2014}. The method identifies significant structural patterns (SSP) and creates a segmented wavelet decomposition (SWD). It is performed as follows.

\begin{enumerate}
\item At each level $j$, the image is decomposed into coefficients $w_j$. We keep only half of the coefficients with the largest magnitudes. This step essentially throws away some of the noise in the image and picks out significant structures.  
\item The image reconstructed by just the significant coefficients at the level $j$ is examined for local maxima. The local maxima define the locations of the SSP in the image. To implement a local maxima detector, we use consider points that have coefficients greater than that of their $8$ nearest neighbors. To implement this efficiently in {\sc Matlab}, we use morphological operation of an image dilation (with a kernel $[1 1 1; 1 1 1; 1 1 1]$) and compare it with the original image: the points constant under the image dilation process are local maxima.
\item Two-dimensional boundaries of each SSP are defined. This is accomplished by using the SSP as initial markers for a watershed algorithm \citep{Beucher1992}. The regions are constrained by the requirement that they must be above a threshold density, $T=0.25$. From the seed point, the region is filled recursively outwards until the threshold boundary is reached.
\end{enumerate}

The output of the algorithm is, for each level $j$, an ID at each pixel indicating which SSP/region it belongs to. A considerable fraction of pixels at a given level may not belong to any segmented group, if there is no significant structure at that length scale.



\subsection{BSR segmentation}\label{sec:BSR}

The BSR segmentation algorithm is a high performance contour detector, combining
local and global image information that transforms contour signals into a hierarchy of regions for image segmentation \citep{Arbelaez2010}. The main ideas of the algorithm are briefly summarized here, the full details are found in \cite{Arbelaez2010}. The method uses local computations of brightness, color, and texture cues at various scales, and couples it to a powerful globalization framework
using spectral clustering. The local cues are computed using oriented gradient operators at every pixel. At each pixel, a neighborhood of a fixed radius is considered, and is split into 2 regions by a line of angle $\theta$ (this part is quite similar to the RHT), and histograms of image properties on both sides of the neighborhood are considered. The gradient is defined by a distance measure between the two histograms. These are then used to define an affinity matrix between pixels. This matrix leads to a generalized eigenproblem, where a fixed number of eigenvectors encode the contour information. The contour signals (which are generally not closed) are then used to construct a general segmentation. First, an initial guess for the segmentations is defined by applying an oriented watershed transformation on the oriented contour signals. Then, these contour regions are placed into a hierarchy by a clustering algorithm. The process is entirely automatic and robust. It has been compared to human-characterizations of image boundaries, and versions of the algorithm exist where humans can input seed points for different regions.

When applied to the ISM, the BSR algorithm identifies different regions in the gas which have different ``texture'', which physically corresponds to different shocked states of the gas, as well as voids (regions with little/no gas). The ISM is supersonically turbulent, which leads to shocks (sharp jumps across pressure and density, and hence sharp boundaries in the image). Simulations reveal that the structure of the ISM consists of ``patches'' of different densities, separated by shock boundaries. Thus it makes sense to apply a brightness, color, and texture based segmentation on the ISM to identify these regions. 

\subsubsection{Retraining the BSR segmentation algorithm}


In the computation of the distances (histogram differences) of the BSR segmentation algorithm, the weights of the different local cues have been optimized such that the algorithm gives good results for the Berkeley segmentation database (BSD). The BSD is a collection of natural images where edges have been detected by humans. 

It is possible to retrain the Berkeley contour detector on data from the ISM (and it is conceivable that different combinations of features work the best for the ISM as opposed to natural images). This involves using annotated data (ISM shocked regions identified by ISM experts) to learn a weight vector for the various features used by the detector. Such data is presently not available. However, we manually experimented with different weights for the various features (by modifying the weights in the {\tt globalPb.m} file), and find that scientific our conclusions (\S~\ref{sec:analysis} and Figure~\ref{fig:a9}) are not changed significantly for reasonable weighting parameters that produce segmentations that visually appear to separate different shocked regions / ``patches''.

In future work, the BSR segmentation algorithm may be retrained on ISM data with shocked regions identified by ISM experts for more accurate results.


\begin{figure}
\centering
\includegraphics[width=0.47\textwidth]{figures/unsharp}
\caption{The unsharp mask used in the RHT analysis.}
\label{fig:unsharp}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.47\textwidth]{figures/diff}
\caption{The difference map of the original image and the unsharp mask in the RHT analysis.}
\label{fig:diff}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.47\textwidth]{figures/RHT}
\caption{Linear coherent structures identified in the data by the RHT. Plotted is the RHT back projection. The three color channels represent the RHT of the 3 velocity channels of the dataset. Green is gas in the rest frame, red is gas moving away from the observer, and blue is gas moving towards the observer.}
\label{fig:rht}
\end{figure}


\section{Results}\label{sec:results}

In this section we describe the results and data products obtained from the RHT, WISE, and BSR algorithms.

The unsharp mask of the data, used for the RHT, is shown in Figure~\ref{fig:unsharp}. This is subtracted from the original image to highlight the small scale features (Figure~\ref{fig:diff}). This step can be considered as a high-pass Fourier filter that suppresses large-scale structure. Figure~\ref{fig:rht} shows the main output of the RHT algorithm. It visualizes the back projection, which is a measure of the strength of the linear structures. The color image is constructed from the 3 velocity channels (A,B,C) in the data. Gas in the rest frame is rendered as green, gas moving away from the observed is in red, and gas moving towards the observer is in blue. There is some degree of coherency between the three color channels, although some of the similarly-oriented features are offset by a spatial translation across the different bands. Figure~\ref{fig:viz} shows another visualization of the RHT. In the left panel, the scaled version of the back projection is subtracted from the original image to highlight the non-linear (diffuse, background) features in the image. In the right panel, the direction of the linear features (the strength is ignored) is portrayed. Large scale, coherent, structure is revealed. The magnetic fields in this turbulent medium are not simply randomly oriented on the smallest scales.

The output of the WISE segmentation is portrayed in Figure~\ref{fig:wise}. We show the blobs and feature points identified at scales $j=3,4,5,6$. The red, green, and blue colors corresponds to the three different velocity channels in the data. These segmented regions define significant diffuse overdensities at particular scales in the image. It ignores underdense regions/voids. Thus is it useful for studying the properties of the dense regions of the ISM at multiple scales. We see that the blobs do not tend to overlap much across the different velocity channels.

The output of the BSR segmentation is shown in Figure~\ref{fig:bsr}. The left panel shows the hierarchical contour boundaries of the different patches that make up the ISM in the 3 velocity channels (A,B,C), shown in green, red, and blue respectively. The right panel shows only the selected significant contours above a threshold. The segmentation provides us with information about the typical size of a shocked patch in the ISM.

We can use these various processed data products to investigate properties of the ISM that were not readily accessible with just the raw data. The focus of this paper is primarily on the computer vision techniques, but we do demonstrate two scientific results that can be learned about the ISM from the computer vision techniques (see \S~\ref{sec:analysis}). We point out that many more detailed investigations can be carried out on the resulting data products to characterize the structure of the ISM. The methods may also be readily applied to any type of astronomical observations (for example, observations of hot X-ray gas in clusters).

First, we investigate the strength of the linear features (proxy for the magnetic fields) as a function of the size of the overdense blobs in which they are embedded (see \S~\ref{sec:analysis}). To do this, we use the results of the RHT and WISE algorithms. At each length scale, $2^{-j}$, we compute the average strength of the linear features (the RHT back projection) in the significant blobs identified by WISE. The conceptual idea is presented in Figure~\ref{fig:RHTvsWISE}, the analysis is presented in \S~\ref{sec:analysis}. The figure shows the original image in one of the channels, the identified blob-like overdense regions at a given length scale, the RHT back projections, and the RHT back projections in each of the blobs. The scientific goal is to investigate whether there are correlations between the size of overdense regions, and the strengths of the linear features, the fibers (which are a proxy for magnetic field strength), that they may contain.

Second, we investigate the volume filling factor of the linear structures in different shocked regions of the gas (see \S~\ref{sec:analysis}). The volume filling factor is calculated by finding the fraction of pixels in a region that have RHT back projections above a threshhold. The shocked regions are identified via the BSR segmentation algorithm. The segments, over-plotted on the original image, is demonstrated in Figure~\ref{fig:RHTvsBSR}. The scientific goal is to gain an understanding of the properties across the shocked patches in the ISM.


\begin{figure*}
\centering
\includegraphics[width=0.47\textwidth]{figures/analysis5}
\includegraphics[width=0.47\textwidth]{figures/analysis8}
\caption{Two different visualizations of the RHT. The left shows the RHT linear features (back projections) subtracted from the original image (only 1 velocity channel is shown). The right shows just the the direction (not strength) of the linear features (yellow is horizontal, pink is vertical)}
\label{fig:viz}
\end{figure*}

\begin{figure}
\centering
\includegraphics[width=0.47\textwidth]{figures/WISE}
\caption{WISE segmentation of the image into multiscale blobs using the wavelet transform. The RGB color channels of the image represent the 3 velocity channels of the dataset. Green=rest frame, red=moving away from observer, blue=moving towards observer. The circles represent the feature points of each identified segment.}
\label{fig:wise}
\end{figure}

\begin{figure*}
\centering
\includegraphics[width=0.47\textwidth]{figures/BSR}
\includegraphics[width=0.47\textwidth]{figures/BSR2}
\caption{Results of the BSR segmentation algorithm. The method identifies hierarchical boundaries (left). The intensity of the colors represent the strength assigned to the boundary. Only boundaries above a certain threshold are kept (right). Again, the RGB color channels of the image represent the 3 velocity channels of the dataset. Green=rest frame, red=moving away from observer, blue=moving towards observer.}
\label{fig:bsr}
\end{figure*}

\begin{figure*}
\centering
\includegraphics[width=0.47\textwidth]{figures/analysis1}
\includegraphics[width=0.47\textwidth]{figures/analysis2} \\
\includegraphics[width=0.47\textwidth]{figures/analysis3}
\includegraphics[width=0.47\textwidth]{figures/analysis4}
\caption{The original (single-channel) image (top left), the significant blob-like regions identified by the WISE decomposition at level $j=3$ (top right), and the RHT linear features in the original image (bottom left) and selected blob-like regions (bottom right). Only one of the velocity channels is shown.}
\label{fig:RHTvsWISE}
\end{figure*}


\begin{figure}
\centering
\includegraphics[width=0.47\textwidth]{figures/analysis7}
\caption{Result of the BSR segmentation algorithm for one of the velocity channels. The regions identified correspond to different shocked regions in this turbulent supersonic ISM environment.}
\label{fig:RHTvsBSR}
\end{figure}


\section{Analysis}\label{sec:analysis}

We show the strength of the linear features (a proxy for magnetic fields) as a function of the blob size in which they are embedded in Figure~\ref{fig:a6}. We find the result that the smallest over-dense regions have the highest field strengths. For comparison, we plot the average magnetic field strengths had the blob-like region been selected at random (we give each blob a random shift between $0$ and the image size). In this case there is no observable trend between linear feature strength and blob size. We draw hundreds of samples to put statistical errors on the expected average value. Thus, the observed trend in the actual data is found to be a significant property of the ISM. The densest regions contain the strongest fibers. This information is not easily recognizable in the raw image.

Second, we study the strengths of the linear structures as a function of the volume filling factor of the shocked regions in which they reside. The shocked regions have been identified with WISE. The observed correlation is shown in Figure~\ref{fig:a9}. Here we see that shocked regions with stronger linear structure strengths have higher filling fractions. This means, if the linear structures trace out the magnetic fields, that compressed, shocked regions squeeze the field lines closer together and strengthen them. This is characteristic of a magneto-hydrodynamic, flux-frozen, plasma, although the result is presently not well-characterized in the supersonic-turbulent regime by modern simulations due to limitations in the numerical methods and computational resources needed to simulate the system. 

\begin{figure}
\centering
\includegraphics[width=0.47\textwidth]{figures/analysis6}
\caption{Strength of linear structures (identified via RHT) as a function of the scale size of blobs of scale $j$ identified by WISE in each of the 3 velocity channels. The strength increases at smaller and smaller spatial scales. For comparison, the average magnetic fields (with errorbars) in the blob-like regions identified by WISE but given a random shift is shown. These randomly selected regions do not show this same trend.}
\label{fig:a6}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.47\textwidth]{figures/analysis9}
\caption{Strength of linear structures (identified via RHT) as a function of the filling factor of linear structures in regions of shock gas identified by the BSR segmentation algorithm. The average strengths increase with volume filling factor. Again, the different colors represent the different velocity channels.}
\label{fig:a9}
\end{figure}


\section{Conclusion}\label{sec:conclusion}

We has successfully applied 3 computer vision techniques to study features in the diffuse ISM. We used a RHT to identify and characterize linear coherent features (which trace the magnetic fields in the ISM). We used a WISE algorithm to identify multiscale blob-like features. And we used the BSR segmentation algorithm to separate different patches of shocked gas. These methods characterize different aspects of the ISM in a very rich and accessible way, resulting in scientifically-valuable data products. The methods are quite general and can extend to any type of astrophysical image data. In the present work, we studied the strength of the linear coherent features as a function of blob size and shock region filling factor. We found that smaller scale blob-like overdensities show stronger linear features (possibly indicative of higher magnetic fields strengths in the contracted parts of the gas). We also show that shocked regions with higher volume filling factor by linear structures also have stronger ones. Both these findings are consistent with the picture that the magnetic fields are flux-frozen in the ISM. The 3 computer vision methods in this paper may be applied to simulation studies of turbulence in the ISM as well, offering a new way the characterize the similarities and differences between observations and simulations. 

We have met the goal of identifying various features in the ISM using modern computer vision techniques and drawing scientific conclusions from their correlation. There are many possibilities for future work. The BSR algorithm for the detection of shocked-regions may be tuned by using training data of edge identification in the ISM from human experts. Such training data is presently lacking. In addition, the search for correlations between features may be made more thorough and automatic. In the present work we have demonstrated two prominent correlations. One could extend the study and, for example, measure the clumpiness of the shocked regions (with WISE and BSR), or the orientation of magnetic fields in adjacent shocked regions, using the present data. Third, one could develop models to derive exact physical values for ISM properties such as the magnetic field strength, using the RHT back projection. Fourth, the methods can be applied to the entire GALFA HI survey data set, to produce a very large catalog of computer-vision identified ISM features for future research. Fifth, the techniques may be applied to other astrophysical data, such as X-ray observations of galaxy clusters, where the hot gas has recently been shown to host linear shocked structures \citep{2013Sci...341.1365S}. Applying computer-vision techniques to characterize astrophysical observations is a subfield still in its infancy, and there is much that can be learned from it.


\section*{Acknowledgements}
This work utilizes data from Galactic ALFA HI (GALFA HI) survey data set obtained with the Arecibo L-band Feed Array (ALFA) on the Arecibo 305m telescope. Arecibo Observatory is part of the National Astronomy and Ionosphere Center, which is operated by Cornell University under Cooperative Agreement with the U.S. National Science Foundation. The GALFA HI surveys are funded by the NSF through grants to Columbia University, the University of Wisconsin, and the University of California. This work also uses the publicly available Berkeley Segmentation Resources (BSR) boundary detector algorithm {\sc Matlab} code.


\bibliography{mybib}{}

%###########################################################################################################
\bsp
\label{lastpage}
\end{document}
%###########################################################################################################
