\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{Data}{3}{0}{2}
\beamer@subsectionintoc {2}{1}{GALFA-HI Survey}{3}{0}{2}
\beamer@sectionintoc {3}{Methods}{4}{0}{3}
\beamer@subsectionintoc {3}{1}{RHT}{4}{0}{3}
\beamer@subsectionintoc {3}{2}{WISE}{5}{0}{3}
\beamer@subsectionintoc {3}{3}{BSR}{6}{0}{3}
\beamer@sectionintoc {4}{Results}{7}{0}{4}
\beamer@subsectionintoc {4}{1}{Data products}{7}{0}{4}
\beamer@subsectionintoc {4}{2}{Data products}{8}{0}{4}
\beamer@subsectionintoc {4}{3}{Data products}{9}{0}{4}
\beamer@subsectionintoc {4}{4}{Analysis}{10}{0}{4}
\beamer@subsectionintoc {4}{5}{Analysis}{11}{0}{4}
\beamer@subsectionintoc {4}{6}{Analysis}{12}{0}{4}
\beamer@subsectionintoc {4}{7}{Analysis}{13}{0}{4}
\beamer@sectionintoc {5}{Conclusion}{14}{0}{5}
\beamer@subsectionintoc {5}{1}{Concluding Remarks}{14}{0}{5}
