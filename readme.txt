Philip Mocz
CS 283 Computer Vision
Harvard University


== Description ==

[Computer vision for the interstellar medium]
Computer vision techniques for structure identification in
observations of the diffuse interstellar medium to characterize their
statistical properties


== Run Instructions ==

This repository does not include the raw data, beause it is large (1GB). It can be downloaded from https://purcell.ssl.berkeley.edu/ 
If you want to obtain the raw data, download GALFA_HI_RA+DEC_060.00+26.35_N.fits from the website and add it to a directory called data/

The script make_data_files.m has been applied to the raw data to extract the relevant slices for our analysis. The output is output in data/images.mat

Run src/BSRdriver.m, src/RHTdriver.m, and WISEdriver.m to run the BSR, RHT, and WISE algorithms on the dataset. These driver scripts save the output of the algorithm in the data/ folder. Then run src/Analysis_driver.m to analyse the outputs of the 3 algorithms.


== Main files and folders ==

data/                 data files are stored and saved here
presentation/         contains 2-min video presentation of the project
  mocz_philip_cs283_presentation.avi
proposal/             initial project proposal
  proposal.pdf
proposal2/            revised project proposal
  proposal.pdf
report/               project writeup
  mocz_philip_cs283_report.pdf
  figures/            contains figures for the report
src/                  project source files
  BSR/                Berkeley Segmentation Resources Matlab files
  RHT/                my implementation of the Rolling Hough Transform
  toolbox_wavelets/   Matlab toolbox wavelet package (Gabriel Peyre)
  WISE/               my implementation of Wavelet-based Image Segmentation and Evaluation
  Analysis_driver.m   analyzes BSR, RHT, WISE outputs
  BSRdriver.m         runs BSR on data set, saves results
  make_data_files.m   process the raw data and extract relevant slices to work with
  RHTdriver.m         runs RHT on data set, saves results
  WISEdriver.m        runs WISE on data set, saves results
  
  
  
Third party code used:
  BSR/                Berkeley Segmentation Resources 
  toolbox_wavelets/   Matlab toolbox wavelet package (Gabriel Peyre)
