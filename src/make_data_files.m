% Philip Mocz, 2014
% processes the GALFA_HI data (1 GB) and extracts slices for analysis and
% inclusion in the bitbucket repository

clear all;
close all;
clc;

%% load the data
% a 'narrow' band cube with 0.18 km/s resolution from -190 to +190 km/s
% and 512 individual RA, dec slices
datacube = fitsread('../data/GALFA_HI_RA+DEC_060.00+26.35_N.fits');
im = mean(datacube(:,:,1023-6:1024+6),3);
[h, w] = size(im);
im2 = mean(datacube(:,:,1024+7:1024+7+13),3);  % moving away observer
im3 = mean(datacube(:,:,1023-7-13:1023-7),3);  % moving towards the observer

save('../data/images.mat','im','im2','im3');
