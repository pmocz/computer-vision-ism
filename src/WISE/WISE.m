function [ regions, mx, my, WC ] = WISE( im, threshold )
%WISE Wavelet-based Image Segmentation and Evaluation (WISE)
%   IM         input image
%   threshold  threshold value for segmentation
%   REGIONS    identified segmentation cells
%   MX         segmentation cells maxima x position
%   MY         segmentation cells maxima y position
%   WC         wavelet coefficients

[h, w] = size(im);

%% perform wavelet transform
Jmin = 3;
options.wavelet_vm = 3;
options.wavelet_type = 'biorthogonal';
options.decomp_type = 'quad';
MS = perform_wavelet_transform(im,Jmin,+1,options);
WC = MS;

Jmax = log2(h/2);

%% keep only the top 1/2 significant coefficients in each level
keep = 2;
% base level
coeffs_min =  MS(1:2^Jmin,1:2^Jmin);
sorted_coeffs = sort(abs(coeffs_min(:)),'descend');
cut = sorted_coeffs(numel(sorted_coeffs)/keep+1);
coeffs_min(abs(coeffs_min) < cut) = 0;
MS(1:2^Jmin,1:2^Jmin) = coeffs_min;

% hierarchical levels
coeffs_C = cell(Jmax-Jmin,1);
coeffs_H = cell(Jmax-Jmin,1);
coeffs_V = cell(Jmax-Jmin,1);
level_mask = cell(Jmax-Jmin,1);
for j = Jmin+1:Jmax
    coeffs_C{j-Jmin} = MS(2^(j-1)+1:2^j,2^(j-1)+1:2^j);
    coeffs_H{j-Jmin} = MS(1:2^(j-1),2^(j-1)+1:2^j);
    coeffs_V{j-Jmin} = MS(2^(j-1)+1:2^j,1:2^(j-1));
    level_mask{j-Jmin} = zeros(size(MS));
    
    % central basis functions (C)
    sorted_coeffs = sort(abs(coeffs_C{j-Jmin}(:)),'descend');
    cut = sorted_coeffs(numel(sorted_coeffs)/keep+1);
    coeffs_C{j-Jmin}(abs(coeffs_C{j-Jmin}) < cut) = 0;
    MS(2^(j-1)+1:2^j,2^(j-1)+1:2^j) = coeffs_C{j-Jmin};
    level_mask{j-Jmin}(2^(j-1)+1:2^j,2^(j-1)+1:2^j) = 1;
    
    % horizontal basis functions (H)
    sorted_coeffs = sort(abs(coeffs_H{j-Jmin}(:)),'descend');
    cut = sorted_coeffs(numel(sorted_coeffs)/keep+1);
    coeffs_H{j-Jmin}(abs(coeffs_H{j-Jmin}) < cut) = 0;
    MS(1:2^(j-1),2^(j-1)+1:2^j) = coeffs_H{j-Jmin};
    level_mask{j-Jmin}(1:2^(j-1),2^(j-1)+1:2^j) = 1;
    
    % vertical basis functions (V)
    sorted_coeffs = sort(abs(coeffs_V{j-Jmin}(:)),'descend');
    cut = sorted_coeffs(numel(sorted_coeffs)/keep+1);
    coeffs_V{j-Jmin}(abs(coeffs_V{j-Jmin}) < cut) = 0;
    MS(2^(j-1)+1:2^j,1:2^(j-1)) = coeffs_V{j-Jmin};
    level_mask{j-Jmin}(2^(j-1)+1:2^j,1:2^(j-1)) = 1;
end


%% identify local maxima, and segmentation using watershedding
regions =  cell(Jmax-Jmin,1);
mx =  cell(Jmax-Jmin,1);
my =  cell(Jmax-Jmin,1);
for j = Jmin+1:Jmax
    
    MSmasked = MS.*level_mask{j-Jmin};
    
    Mrecover = perform_wavelet_transform(MSmasked,Jmin,-1,options);
    Mrecover(1,:) = max(Mrecover(:));
    Mrecover(end,:) = max(Mrecover(:));
    Mrecover(:,1) = max(Mrecover(:));
    Mrecover(:,end) = max(Mrecover(:));
    
    % identify local maxima
    maxima = imdilate(Mrecover, ones(3,3))==Mrecover;
    maxima(1,:) = 0;
    maxima(end,:) = 0;
    maxima(:,1) = 0;
    maxima(:,end) = 0;
    maxima = maxima & (maxima ~= circshift(maxima,[1 0]));
    maxima = maxima & (maxima ~= circshift(maxima,[-1 0]));
    maxima = maxima & (maxima ~= circshift(maxima,[0 1]));
    maxima = maxima & (maxima ~= circshift(maxima,[0 -1]));
    maxima = maxima.*(Mrecover>=threshold);
    
    [mx{j-Jmin},my{j-Jmin}] = find(maxima);
    midx = find(maxima);
    N_maxima = length(mx{j-Jmin});
    mid = 1:N_maxima;
    
    % watershed
    regions{j-Jmin} = zeros(size(im));
    regions{j-Jmin}(midx) = mid;
    shifts = {[1 0], [-1 0], [0 1], [0 -1]};
    for i = 1:h/2
        for s = 1:4
            sh = shifts{s};
            regions_sh = circshift(regions{j-Jmin},sh);
            Mrecover_sh = circshift(Mrecover,sh);
            regions_sh = regions_sh.*(regions{j-Jmin}==0 & Mrecover_sh >= Mrecover & Mrecover_sh >= threshold);
            regions{j-Jmin} = max(regions{j-Jmin},regions_sh);
        end
    end
end


end

