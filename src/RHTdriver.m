% Philip Mocz, 2014
% driver file for Rolling Hough Transform

clear all;
close all;
clc;

addpath('RHT/');


%% load the data
load('../data/images.mat','im','im2','im3');
[h, w] = size(im);
im_rgb = zeros(h,w,3);
im_rgb(:,:,1) = im;
im_rgb(:,:,2) = im2;
im_rgb(:,:,3) = im3;
im_rgb = im_rgb/max(im_rgb(:));
imshow(im_rgb,[])


%% perform RHT
R_K = 4;
R_W = 32;
Z = 0.6;

[R,R_bp,unsharp] = RollingHoughTransform( im, R_K, R_W, Z);
[R2,R2_bp,unsharp2] = RollingHoughTransform( im2, R_K, R_W, Z);
[R3,R3_bp,unsharp3] = RollingHoughTransform( im3, R_K, R_W, Z);

imshow(R_bp,[])


%% plot back projections
R_bp_rgb = zeros(h,w,3);
R_bp_rgb(:,:,1) = R_bp;
R_bp_rgb(:,:,2) = R2_bp;
R_bp_rgb(:,:,3) = R3_bp;
R_bp_rgb = R_bp_rgb/max(R_bp_rgb(:));
imshow(R_bp_rgb,[])


%% plot an example unsharp mask for report
figure;
imshow(unsharp,[]);
print('-depsc','../report/figures/unsharp.eps');
figure;
imshow(im-unsharp,[-2 4]);
print('-depsc','../report/figures/diff.eps');


%% save output for further processing
save('../data/RHT.mat','R','R_bp','R2','R2_bp','R3','R3_bp');
