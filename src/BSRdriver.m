% Philip Mocz, 2014
% driver file for applying Berkeley Segmentation Resources

clear all;
close all;
clc;

addpath('BSR/grouping/lib/');


%% load the data
load('../data/images.mat','im','im2','im3');
[h, w] = size(im);


%% save images
imwrite(im/max(im(:)),'../data/tmp/BSR_im_in.png');
imwrite(im2/max(im2(:)),'../data/tmp/BSR_im2_in.png');
imwrite(im3/max(im3(:)),'../data/tmp/BSR_im3_in.png');

clear all; close all; clc;


%% compute globalPb on image (5Gb of RAM required), & Hierarchical Regions

clear all; close all; clc;
% 1. compute globalPb on image
imgFile = '../data/tmp/BSR_im_in.png';
outFile = '../data/tmp/BSR_out_tmp.mat';
gPb_orient = globalPb(imgFile, outFile);
% 2. compute Hierarchical Regions
ucm = contours2ucm(gPb_orient, 'doubleSize');
save('../data/BSR.mat','ucm');

clear all; close all; clc;
% 1. compute globalPb on image
imgFile = '../data/tmp/BSR_im2_in.png';
outFile = '../data/tmp/BSR_out_tmp.mat';
gPb_orient = globalPb(imgFile, outFile);
% 2. compute Hierarchical Regions
ucm2 = contours2ucm(gPb_orient, 'doubleSize');
save('../data/BSR2.mat','ucm2');

clear all; close all; clc;
% 1. compute globalPb on image
imgFile = '../data/tmp/BSR_im3_in.png';
outFile = '../data/tmp/BSR_out_tmp.mat';
gPb_orient = globalPb(imgFile, outFile);
% 2. compute Hierarchical Regions
ucm3 = contours2ucm(gPb_orient, 'doubleSize');
save('../data/BSR3.mat','ucm3');


%% 3. usage example
clear all;close all;clc;

%load double sized ucm
load('../data/BSR.mat','ucm');

% convert ucm to the size of the original image
ucm_h = ucm(3:2:end, 3:2:end);

% get the boundaries of segmentation at scale k in range [0 1]
k = 0.23;
bdry = (ucm_h >= k);

% get superpixels at scale k without boundaries:
labels2 = bwlabel(ucm <= k);
labels = labels2(2:2:end, 2:2:end);

figure;imshow('../data/tmp/BSR_im_in.png');
figure;imshow(ucm_h);
figure;imshow(bdry);
figure;imshow(labels,[]);colormap(jet);

