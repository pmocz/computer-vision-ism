% Philip Mocz, 2014
% driver file for analyzing the output of BSR, RHT, and WISE
% run BSRdriver.m, RHTdriver.m, and WISEdriver.m first to generate the
% output data

clear all;
close all;
clc;
rng=(47);


%% load data and processed data products
load('../data/images.mat','im','im2','im3');
[h, w] = size(im);

load('../data/RHT.mat','R','R_bp','R2','R2_bp','R3','R3_bp');
load('../data/WISE.mat','regions', 'mx', 'my', 'regions2', 'mx2', 'my2', 'regions3', 'mx3', 'my3');
load('../data/BSR.mat','ucm');
load('../data/BSR2.mat','ucm2');
load('../data/BSR3.mat','ucm3');


%% crop border
cr = 32;
crop = cr+1:h-cr;
im = im(crop,crop);
im2 = im2(crop,crop);
im3 = im3(crop,crop);
R = R(:,crop,crop);
R_bp = R_bp(crop,crop);
R2 = R2(:,crop,crop);
R2_bp = R2_bp(crop,crop);
R3 = R3(:,crop,crop);
R3_bp = R3_bp(crop,crop);
for i = 1:numel(regions)
    regions{i} = regions{i}(crop,crop);
    mx{i} = mx{i} - cr;
    my{i} = my{i} - cr;
    regions2{i} = regions2{i}(crop,crop);
    mx2{i} = mx2{i} - cr;
    my2{i} = my2{i} - cr;
    regions3{i} = regions3{i}(crop,crop);
    mx3{i} = mx3{i} - cr;
    my3{i} = my3{i} - cr;
end
h = h - 2*cr;
w = w - 2*cr;


%% process ucm data

% convert ucm to the size of the original image and crop
ucm = ucm(3:2:end, 3:2:end);
ucm = ucm(crop,crop);
ucm2 = ucm2(3:2:end, 3:2:end);
ucm2 = ucm2(crop,crop);
ucm3 = ucm3(3:2:end, 3:2:end);
ucm3 = ucm3(crop,crop);

% get the boundaries of segmentation at scale k in range [0 1]
k = 0.23;
bdry = (ucm >= k);
bdry2 = (ucm2 >= k);
bdry3 = (ucm3 >= k);

% get superpixels at scale k without boundaries:
labels = bwlabel(ucm <= k);
labels2 = bwlabel(ucm2 <= k);
labels3 = bwlabel(ucm3 <= k);


%% plot the input data
im_rgb = zeros(h,w,3);
im_rgb(:,:,1) = im2;
im_rgb(:,:,2) = im;
im_rgb(:,:,3) = im3;
im_rgb = im_rgb/max(im_rgb(:));
figure;
imshow(im_rgb,[])
print('-depsc','../report/figures/data.eps');


%% plot the RHT back projections
R_bp_rgb = zeros(h,w,3);
R_bp_rgb(:,:,1) = R2_bp;
R_bp_rgb(:,:,2) = R_bp;
R_bp_rgb(:,:,3) = R3_bp;
R_bp_rgb = R_bp_rgb/max(R_bp_rgb(:));
figure;
imshow(R_bp_rgb,[])
print('-depsc','../report/figures/RHT.eps');


%% plot the WISE segmentation
figure;
regions_rbg = cell(4,1);
for level=1:4
    regions_rbg{level} = zeros(h,w,3);
    rgb = label2rgb(regions{level},'cool',[0 0 0], 'shuffle');
    regions_rbg{level}(:,:,2) = rgb(:,:,1);
    rgb = label2rgb(regions2{level},'cool',[0 0 0], 'shuffle');
    regions_rbg{level}(:,:,1) = rgb(:,:,1);
    rgb = label2rgb(regions3{level},'cool',[0 0 0], 'shuffle');
    regions_rbg{level}(:,:,3) = rgb(:,:,1);
    subplot(2,2,level,'Position',[0.05+0.4*mod(level+1,2) 0+0.5*(level<3) 0.48 0.48])
    imshow(regions_rbg{level}/256,[]);
    hold on
    plot(my{level},mx{level},'go','linewidth',1)
    plot(my2{level},mx2{level},'ro','linewidth',1)
    plot(my3{level},mx3{level},'bo','linewidth',1)
end
print('-depsc','../report/figures/WISE.eps');


%% plot BSR
ucm_rgb = zeros(h,w,3);
ucm_rgb(:,:,1) = ucm2/max(ucm2(:));
ucm_rgb(:,:,2) = ucm/max(ucm(:));
ucm_rgb(:,:,3) = ucm3/max(ucm3(:));
figure;imshow(ucm_rgb,[]);
print('-depsc','../report/figures/BSR.eps');

bdry_rgb = zeros(h,w,3);
bdry_rgb(:,:,1) = bdry2/max(ucm2(:));
bdry_rgb(:,:,2) = bdry/max(ucm(:));
bdry_rgb(:,:,3) = bdry3/max(ucm3(:));
figure;imshow(bdry_rgb,[]);
print('-depsc','../report/figures/BSR2.eps');


%% RHT vs WISE: basic plots
level = 1;
figure;
imshow(im,[]);
colormap(hot);
print('-depsc','../report/figures/analysis1.eps');
figure;
imshow((regions{level}>0).*im,[]);
colormap(hot);
print('-depsc','../report/figures/analysis2.eps');
figure;
imshow(R_bp,[]);
colormap(hot);
print('-depsc','../report/figures/analysis3.eps');
figure;
imshow((regions{level}>0).*R_bp,[]);
colormap(hot);
print('-depsc','../report/figures/analysis4.eps');
figure;
imshow(im./(0.01+R_bp),[]);
colormap(hot);
print('-depsc','../report/figures/analysis5.eps');


%% RHT vs WISE: statistics
n_trials = 300;
avg_R_bp_level = zeros(5,1);
avg_R2_bp_level = zeros(5,1);
avg_R3_bp_level = zeros(5,1);
avg_R_bp_level_control = zeros(5,n_trials);
avg_R2_bp_level_control = zeros(5,n_trials);
avg_R3_bp_level_control = zeros(5,n_trials);
for level=1:5
    % calculate average strength of lines in region
    avg_R_bp_level(level) = sum((regions{level}(:)>0).*R_bp(:))/sum(regions{level}(:)>0);
    avg_R2_bp_level(level) = sum((regions2{level}(:)>0).*R2_bp(:))/sum(regions2{level}(:)>0);
    avg_R3_bp_level(level) = sum((regions3{level}(:)>0).*R3_bp(:))/sum(regions3{level}(:)>0);
    % calculate average strength of lines in random region
    for i = 1:n_trials
        regions_control = circshift(regions{level},[randi(h),randi(w)]);
        avg_R_bp_level_control(level,i) = sum((regions_control(:)>0).*R_bp(:))/sum(regions_control(:)>0);
        regions_control = circshift(regions2{level},[randi(h),randi(w)]);
        avg_R2_bp_level_control(level,i) = sum((regions_control(:)>0).*R2_bp(:))/sum(regions_control(:)>0);
        regions_control = circshift(regions3{level},[randi(h),randi(w)]);
        avg_R3_bp_level_control(level,i) = sum((regions_control(:)>0).*R3_bp(:))/sum(regions_control(:)>0);
    end
end
% normalize
norm = mean(avg_R_bp_level_control(:));
norm2 = mean(avg_R2_bp_level_control(:));
norm3 = mean(avg_R3_bp_level_control(:));
avg_R_bp_level_control = avg_R_bp_level_control / norm;
avg_R2_bp_level_control = avg_R2_bp_level_control / norm2;
avg_R3_bp_level_control = avg_R3_bp_level_control / norm3;
% plot figure
figure;
loglog(2.^(-(3:7)),avg_R_bp_level/norm,'go','linewidth',2)
hold on
loglog(2.^(-(3:7)),avg_R2_bp_level/norm2,'ro','linewidth',2)
loglog(2.^(-(3:7)),avg_R3_bp_level/norm3,'bo','linewidth',2)
errorbar(2.^(-(3:7)),mean(avg_R_bp_level_control,2),std(avg_R_bp_level_control'),'ks','linewidth',2)
legend('channel A','channel B','channel C','random');
%errorbar(2.^(-(3:7)),mean(avg_R2_bp_level_control,2),std(avg_R2_bp_level_control'),'ks','linewidth',2)
%errorbar(2.^(-(3:7)),mean(avg_R3_bp_level_control,2),std(avg_R3_bp_level_control'),'ks','linewidth',2)
axis([10^-2.5 10^-0.5 10^-0.1 10^0.3])
print('-depsc','../report/figures/analysis6b.eps');
xlabel('scale $2^{-j}$','interpreter','latex','fontsize',14);
ylabel('strength of linear structures','interpreter','latex','fontsize',14);
print('-depsc','../report/figures/analysis6.eps');


%% RHT vs BSR: basic plots
figure;
imshow(imsharpen(im2).*(bdry2~=1)+max(im2(:)).*(bdry2==1),[])
print('-depsc','../report/figures/analysis7.eps');


%% RHT vs BSR: statistics

% plot just the directionality of the linear features, not the strengths
[mx,dir2] = max(R2);
dir2 = reshape(dir2.*(mx>0),[h w])*pi/70;
my_cmap = spring(256);
my_cmap(1,:) = 0;
figure
imshow(pi/2-abs(dir2-pi/2),[0 pi/2]);colormap(my_cmap);
print('-depsc','../report/figures/analysis8.eps');
%%

% filling factor vs field strength
nlabels = max(labels(:));
avgb = zeros(nlabels,1);
fillfac = zeros(nlabels,1);
for i = 1:nlabels
    vol = sum(labels(:)==i);
    if vol > h*w/100
        fillfac(i) = sum((labels(:)==i) & R_bp(:)>0.05) / vol;
        avgb(i) = sum(sum((circshift(labels,[0 0])==i).*R_bp.*(R_bp>0.05)))/ sum((labels(:)==i) & R_bp(:)>0.05);
    end
end
figure;
plot(fillfac(fillfac>0),avgb(fillfac>0),'go','linewidth',2)

nlabels = max(labels2(:));
avgb = zeros(nlabels,1);
fillfac = zeros(nlabels,1);
for i = 1:nlabels
    vol = sum(labels2(:)==i);
    if vol > h*w/100
        fillfac(i) = sum((labels2(:)==i) & R2_bp(:)>0.05) / vol;
        avgb(i) = sum(sum((circshift(labels2,[0 0])==i).*R2_bp.*(R2_bp>0.05)))/ sum((labels2(:)==i) & R2_bp(:)>0.05);
    end
end
hold on
plot(fillfac(fillfac>0),avgb(fillfac>0),'ro','linewidth',2)

nlabels = max(labels3(:));
avgb = zeros(nlabels,1);
fillfac = zeros(nlabels,1);
for i = 1:nlabels
    vol = sum(labels3(:)==i);
    if vol > h*w/100
        fillfac(i) = sum((labels3(:)==i) & R2_bp(:)>0.05) / vol;
        avgb(i) = sum(sum((circshift(labels3,[0 0])==i).*R2_bp.*(R2_bp>0.05)))/ sum((labels3(:)==i) & R2_bp(:)>0.05);
    end
end
hold on
plot(fillfac(fillfac>0),avgb(fillfac>0),'bo','linewidth',2)
print('-depsc','../report/figures/analysis9b.eps');
xlabel('filling factor','interpreter','latex','fontsize',14);
ylabel('strength of linear structures','interpreter','latex','fontsize',14);
print('-depsc','../report/figures/analysis9.eps');





