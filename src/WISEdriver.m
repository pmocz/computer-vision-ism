% Philip Mocz, 2014
% driver file for Wavelet-based Image Segmentation and Evaluation (WISE)

clear all;
close all;
clc;

addpath('WISE/');
addpath('toolbox_wavelets/');
addpath('toolbox_wavelets/toolbox/');


%% load the data
load('../data/images.mat','im','im2','im3');
[h, w] = size(im);


%% wavelet decomposition, segmentation
threshold = 0.25;
[regions, mx, my, WC] = WISE(im,threshold);
[regions2, mx2, my2, WC2] = WISE(im2,threshold);
[regions3, mx3, my3, WC3] = WISE(im3,threshold);


%% plot segmentation
level = 3;

rgb = label2rgb(regions{level},'bone',[0 0 0], 'shuffle');
figure;
imshow(rgb);
hold on
plot(my{level},mx{level},'ro','linewidth',2)


%% plot wavelet coeffs
figure;
imshow(log(WC),[]);
print('-depsc','../report/figures/wavelet_coeffs.eps');


%% plot wavelet functions
Jmin = 3;
options.wavelet_vm = 3;
options.wavelet_type = 'biorthogonal';
options.decomp_type = 'quad';
WCmod = 0*WC;
WCmod(10,6)=1;
WCmod(14,14)=1;
WCmod(6,10)=1;
WF = perform_wavelet_transform(WCmod,Jmin,-1,options);
imshow(WF,[])
print('-depsc','../report/figures/wavelet_functions.eps');


%% save output for further processing
save('../data/WISE.mat','regions', 'mx', 'my', 'regions2', 'mx2', 'my2', 'regions3', 'mx3', 'my3');

