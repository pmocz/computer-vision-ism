function [R, R_bp, unsharp] = RollingHoughTransform( im, R_K, R_W, Z )
%ROLLINGHOUGHTRANSFORM performs a RollingHoughTransform on the input image
%   IM       image: size m by n
%   R_K      smoothing kernel radius for unsharp mask (D_K = 2*R_K)
%   R_W      radius pixel neighborhood for RHT (D_W = 2*R_W)
%   Z        threshold [0,1] for line detection
%   R        RHT result of dimension n_theta by m by n
%   R_bp     back projection (integral over theta) of R
%   unsharp  Unsharp mask

[h, w] = size(im);

% set up theta bins
n_theta = ceil(pi*sqrt(2)/2*(R_W-1));
theta = linspace(0,pi,n_theta+1);
theta = theta(1:end-1);
dtheta = pi/n_theta;

% create theta masks
theta_mask = zeros((2*R_W+1)^2,n_theta);
x = -R_W:R_W;
y = 0*x;
for k = 1:n_theta
    theta_mask_k = zeros(2*R_W+1,2*R_W+1);
    xx = round(x*cos(theta(k)) - y*sin(theta(k)));
    yy = round(x*sin(theta(k)) + y*cos(theta(k)));
    for i = 1:2*R_W+1
        theta_mask_k(xx(i)+R_W+1,yy(i)+R_W+1) = 1;
    end
    theta_mask_k = theta_mask_k(:)/sum(theta_mask_k(:));
    theta_mask(:,k) = theta_mask_k;
end

% subtract unsharp mask
unsharp = conv2(im,fspecial('disk', R_K),'same');
diff = im-unsharp;
bitmask = diff > 0;

% main loop
R = zeros(n_theta,h,w);
fprintf('starting RHT ... \n');
for i = 1+R_W:h-R_W;
    for j = 1+R_W:w-R_W;
        nbh = bitmask(i-R_W:i+R_W,j-R_W:j+R_W);
        R(:,i,j) = sum(repmat(nbh(:),1,n_theta).*theta_mask,1);
    end
    fprintf('processed: %03.1f %%\n',100*(i-R_W)/(w-2*R_W));
end
fprintf(' ... done! \n');

% apply threshold
R = R - Z;
R(R<0) = 0;

% calculate back projection
R_bp = reshape(sum(R,1),h,w)*dtheta;

end

